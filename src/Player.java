import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Player {
    private String playerName;
    private Board board;
    public boolean loss;
    private ShipList fleet;
    private AttackRecord record;
    private boolean human;

    public Player(String aname, boolean h) {
        playerName = aname;
        board = new Board();
        fleet = new ShipList();
        record = new AttackRecord();
        human = h;

    }

    public void displaytoMe() {
        board.displaytoMe();
    }

    public void displaytoEnemy() {
        board.displaytoEnemy();
    }
    public void placeShip(int m, int n) {
        int x = 0;
        int y = 0;
        boolean valid = false;
        boolean alignment = false;
        while(valid == false)
        {
            if(human)
            {
                String ship = "";
                if(n==1)
                {
                    ship = "aircraft carrier";
                }
                if(n==2)
                {
                    ship = "battleship";
                }
                if(n==3)
                {
                    ship = "destroyer";
                }
                if(n==4)
                {
                    ship = "submarine";
                }
                if(n==5)
                {
                    ship = "patrolboat";
                }
                try
                {
                    System.out.println("Wstaw " + ship + ". Ma " + m + " jednostke/ki długości");
                    System.out.println("Czy orientacja powinna być: 1 - pozioma, or 2 - pionowa?");
                    Scanner orientation = new Scanner(System.in);
                    int a = orientation.nextInt();
                    if(a != 1 && a != 2)
                    {
                        InputMismatchException e = new InputMismatchException();
                        throw e;
                    }
                    else if(a == 1)
                    {
                        alignment = true;
                    }
                    else if(a == 2)
                    {
                        alignment = false;
                    }
                    System.out.println("Podaj punkt początkowy na osi pionowej");
                    Scanner vertical = new Scanner(System.in);
                    x = vertical.nextInt();
                    System.out.println("Podaj punkt początkowy na osi poziomej");
                    Scanner horizontal = new Scanner(System.in);
                    y = horizontal.nextInt();
                    valid = board.checkAvailibility(alignment, m, x, y);
                    if (valid == false)
                    {
                        System.out.println("Na planszy nie ma miejsca na taki statek. Umieść statek w innym miejscu.");
                    }
                }
                catch (InputMismatchException e)
                {
                    System.out.println("Wprowadzone informcje są nieprawidłowe. Proszę spróbować ponownie");
                    valid = false;
                }

            }
            else
            {
                Random random = new Random();
                alignment = random.nextBoolean();
                x = (int) (Math.random()*10);
                y = (int) (Math.random()*10);
                valid = board.checkAvailibility(alignment, m, x, y);
            }
        }
        board.addShip(alignment, m, x, y, n);
    }
    public Coordinates attack() {
        System.out.println("Kolej uzytkownika " + playerName + " na atak");
        int x = 0;
        int y = 0;
        boolean redundant = true;

        if(human)
        {
            while(redundant)
            {
                try
                {
                    System.out.println("Proszę wybrać współrzędną na osi pionowej");
                    Scanner targetx = new Scanner(System.in);
                    x = targetx.nextInt();
                    if (x < 0 || x > 9)
                    {
                        InputMismatchException f = new InputMismatchException();
                        throw f;
                    }
                    System.out.println("Proszę wybrać współrzędną na osi poziomej");
                    Scanner targety = new Scanner(System.in);
                    y = targety.nextInt();
                    if (y < 0 || y > 9)
                    {
                        InputMismatchException f = new InputMismatchException();
                        throw f;
                    }
                    if(record.isRedundant(x, y))
                    {
                        System.out.println("Współrzędne zostały już wcześniej wybrane. Proszę spróbuj ponownie");
                    }
                    else
                    {
                        redundant = false;
                    }
                }
                catch (InputMismatchException f)
                {
                    System.out.println("Współrzędna nie jest prawidłowa. Musi to być liczba całkowita z zakresu od 0 do 9.");
                    redundant = true;
                }
            }
        }
        else
        {
            while (redundant)
            {
                x = (int) (Math.random()*10);
                y = (int) (Math.random()*10);
                if(record.isRedundant(x, y) == false)
                {
                    redundant = false;
                }
            }
        }
        Coordinates attackCoordinates = new Coordinates(x, y);
        record.addAttack(attackCoordinates);
        return attackCoordinates;
    }

    public void receiveAttack(Coordinates c) {
        int status = board.checkHit(c);
        if (status == 0)
        {
            System.out.println("Atak nie powiódł się");
            board.receiveMiss(c);
        }
        else
        {
            System.out.println("Statek gracza "+ playerName + " został trafiony.");
            board.receiveHit(c);
            fleet.shipHit(status);
        }
    }

    public boolean hasLost() {
        if (fleet.isLost())
        {
            loss = true;
        }
        return loss;

    }
}
