public class Game {
    Player Human;
    Player Computer;

    public Game() {
        Human = new Player("Gracz", true);
        Computer = new Player("Komputer", false);
    }

    public void play() {
        Human.placeShip(5, 1);
        Human.displaytoMe();
        Human.placeShip(4, 2);
        Human.displaytoMe();
        Human.placeShip(3, 3);
        Human.displaytoMe();
        Human.placeShip(3, 4);
        Human.displaytoMe();
        Human.placeShip(2, 5);
        Human.displaytoMe();
        Computer.placeShip(5, 1);
        Computer.placeShip(4, 2);
        Computer.placeShip(3, 3);
        Computer.placeShip(3, 4);
        Computer.placeShip(2, 5);

        Computer.displaytoEnemy();

        while (Human.hasLost() == false && Computer.hasLost() == false) {
            Computer.receiveAttack(Human.attack());
            Computer.displaytoEnemy();
            if (Computer.hasLost()) {
                break;
            }
            Human.receiveAttack(Computer.attack());
            Human.displaytoMe();
        }

        if (Computer.hasLost()) {
            System.out.println("Gracz komputer przegrał.");
        }

        if (Human.hasLost()) {
            System.out.println("Przegrałeś.");
        }
    }
}