#Opis
Poprzez utworzenie instancji klasy Game w Drive  zostają utworzone dwa obiekty Player –w tym przypadku nazwane Human i Computer. Human ma wartość boolean ustawioną na true, by mógł sterować nim człowiek, natomiast Computer false. W przypadku ustawienia obu wartości na false gra jest automatyczna, bez udziału człowieka.
Każdy z graczy tworzy instancje nowych obiektów Board, AttackReckord oraz ShipList – poprzez który utworzy pięć obiektów – statków

Następnie drive wywołuje metodę Game.play, w ramach której gracze rozmieszczają swoje statki, po czym pętla while określi, kiedy któryś z graczy przegrał. Ataki wykonywane będą na zmianę.

Podczas rozmieszczania statków gracz wprowadza trzy zestawy danych wejściowych:
* 1 lub 2 – określającego, czy położenie jest poziome/pionowe
* Wartość int dla początkowej współrzędnej pionowej
* Wartość int dla początkowej wspołrzędnej poziomej.

<img src=”” width=”50%” height=”50%”>

Jeżeli któraś z wprowadzonych danych nie jest zgodna z podanym foramtem zostanie zgłoszony InputMismatchException, a użytkownik ponownie wprowadzi dane.

Gdy format będzie poprawny zostanie wywołana metoda checkAvailibity klasy Board. Sprawdzi, czy statek fizycznie może zmieścić się na planszy. Gdy nie – gracz wprowadzi dane raz jeszcze. W przypadku komputera dane generowane są losowo.

Gdy będzie kolej na atak poprzez gracza, będzie on musiał wprowadzić współrzędne celu. Jeżeli są poza zasięgiem tablicy, wprowadzi je ponownie. Gdy nie wykraczają poza odpowiedni zakres zostanie wywołana metoda isRedundant klasy AttackReckord, która śledzi wszytskie ataki wykonane przez gracza.

Wspołrzędne ataku zostają zwrócone poprzez obiekt klasy Coordinates, by możliwe było zwrócenie dwóch w tym samym czasie.

Po otrzymaniu ataku wywoływana jest metoda checkHit klasy Board, określająca czy atak trafił w statek. Gdy tak – metoda receiveHit, gdy nie receiveMiss. Gdy statek zostanie trafiony wywoływana jest metoda shipHit z klasy ShipList.

Na koniec gry każdy statek (obiekt Ship) określi, kiedy zostanie zatopiony, poprzez porównanie liczby otrzymanych trafień do swojej długości.
Gdy wszystkie ze statków zwrócą true z metody isSunk, metoda isLost również zwróci true. Pętla while zostanie zakończona i jeden z graczy przegra.
